/*
╔══════════════════════════════════════════════════════════════════════════════╗
║ Financier Core Libary (Unofficial)                                           ║
╟──────────────────────────────────────────────────────────────────────────────╢
║ Author: Adam Romzek <asromzek@gmail.com>                                     ║
║ Description: Financier core libary.                                          ║
╚══════════════════════════════════════════════════════════════════════════════╝
*/

// ##################################################
//  Project imports.
// ##################################################

import { Auth } from './auth';
import { CurrentBudget } from './current-budget';
import { Database } from './database';

// ##################################################
//  Utility imports.
// ##################################################

import { Currency } from './utilities/currency';

// ##################################################
//  Project main exports.
// ##################################################

export { Auth } from './auth';
export { Database } from './database';
export { CurrentBudget } from './current-budget';
export { DbController } from './db-controller';

// ##################################################
//  Project data exports.
// ##################################################

export { Account } from './data/account';
export { BudgetOpened } from './data/budget-opened';
export { Budget } from './data/budget';
export { Category } from './data/category';
export { MasterCategory } from './data/master-category';
export { MonthCategory } from './data/month-category';
export { Month } from './data/month';
export { Payee } from './data/payee';
export { SplitTransaction } from './data/split-transaction';
export { Transaction } from './data/transaction';

// ##################################################
//  Project manager exports.
// ##################################################

export { BudgetManager } from './managers/budget-manager';
export { MonthManager } from './managers/month-manager';

// ##################################################
//  Project utility exports.
// ##################################################

export { Currencies } from './utilities/currencies';
export { Currency } from './utilities/currency';
export { DefaultCategories } from './utilities/default-categories';

// ##################################################
//  FinancierCore class definition.
// ##################################################

export class FinancierCoreUnofficial {
  //
  // ==================================================
  //  Constructor.
  // ==================================================

  constructor () {
    this._currency = new Currency();
    this._db = new Database().get();
    this._current = new CurrentBudget(this._db, this._currency);
    this._auth = new Auth(this._db);
  }

  // ==================================================
  //  Destructor.
  // ==================================================

  destroy () {
  }

  // ==================================================
  //  Authentication object.
  // ==================================================

  get auth () {
    return this._auth;
  }

  // ==================================================
  //  Current budget manager.
  // ==================================================

  get current () {
    return this._current;
  }

  // ==================================================
  //  Currency object.
  // ==================================================

  get currency () {
    return this._currency;
  }

  // ==================================================
  //  Pouch database object.
  // ==================================================

  get db () {
    return this._db;
  }
}
