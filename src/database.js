/*
╔══════════════════════════════════════════════════════════════════════════════╗
║ Financier Database Manager                                                   ║
╟──────────────────────────────────────────────────────────────────────────────╢
║ Author: Adam Romzek <asromzek@gmail.com>                                     ║
║ Description: Financier database manager libary.                              ║
╚══════════════════════════════════════════════════════════════════════════════╝
*/

// ##################################################
//  Library imports.
// ##################################################

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import PouchDB from 'pouchdb';

// ##################################################
//  Project imports.
// ##################################################

import { Budget as budget } from './data/budget';
import { BudgetOpened as budgetOpened } from './data/budget-opened';
import { BudgetManager } from './managers/budget-manager';

// ##################################################
//  Database function definition.
// ##################################################

export function Database () {
  const Budget = budget();
  const BudgetOpened = budgetOpened();

  this.get = () => {
    let db, sync, changes;

    const $syncChanged = new BehaviorSubject(false);
    const $pouchChanged = new Subject();
    const $dataDestroyed = new Subject();

    create();

    return {
      budget,
      budgets: budgets(),
      budgetsOpened: budgetsOpened(),
      get _pouch () {
        return db;
      },
      destroy,
      sync: {
        start: startSync,
        cancel: cancelSync
      },
      $syncChanged,
      $pouchChanged,
      $dataDestroyed
    };

    function cancelSync () {
      if (sync) {
        sync.cancel();
      }

      if (changes) {
        changes.cancel();
      }
    }

    function startSync (dbName, isValidSub) {
      const options = {
        live: true,
        retry: true,
        batch_size: 500
      };

      cancelSync();

      const host = window.location.host;

      if (isValidSub) {
        sync = db.sync(`https://${host}/db/${dbName}`, options)
          .on('paused', function () {
            $syncChanged.next(false); // User went offline.
          });
      } else {
        sync = PouchDB.replicate(`https://${host}/db/${dbName}`, db, options)
          .on('paused', function () {
            $syncChanged.next(false); // User went offline.
          });
      }

      sync
        .on('change', function (info) {
          $syncChanged.next(true);
        })
        .on('active', function () {
          $syncChanged.next(true); // replicate resumed (e.g. user went back online)
        })
        .on('denied', function (info) {
          $syncChanged.next(false); // a document failed to replicate (e.g. due to permissions)
        })
        .on('complete', function (info) {
          $syncChanged.next(false); // handle complete
        })
        .on('error', function (err) {
          console.log('sync error', err);
          $syncChanged.next(false); // handle error
        });

      changes = db.changes({
        since: 'now',
        live: true,
        include_docs: true
      }).on('change', change => {
        $pouchChanged.next(change); // received a change
      }).on('error', err => {
        console.log('error subscribing to changes feed', err); // handle errors
      });
    }

    function destroy () {
      cancelSync();
      console.log('Destroying database');
      return db.destroy().then(() => {
        $dataDestroyed.next(true);
        create();
      });
    }

    function create () {
      if (process.env.NODE_ENV === 'development') {
        db = new PouchDB('financier-dev', {
          adapter: null,
          size: 50,
          auto_compaction: true
        });
        console.log('Using database: financier-dev');
      } else {
        db = new PouchDB('financier', {
          adapter: null,
          size: 50,
          auto_compaction: true
        });
        console.log('Using database: financier');
      }
    }

    function budget (budgetId) {
      return BudgetManager(db, budgetId);
    }

    function budgets () {
      function put (budget) {
        return db.put(budget.toJSON()).then(res => {
          budget._rev = res.rev;
        });
      }

      function get (id) {
        return db.get(`${Budget.prefix}${id}`).then(b => {
          const budget = new Budget(b);
          budget.subscribe(put);

          return budget;
        });
      }

      function all () {
        return db.allDocs({
          include_docs: true,
          startkey: Budget.startKey,
          endkey: Budget.endKey
        }).then(res => {
          const budgets = [];
          for (let i = 0; i < res.rows.length; i++) {
            const budget = new Budget(res.rows[i].doc);
            budget.subscribe(put);
            budgets.push(budget);
          }

          return budgets;
        });
      }

      return {
        all,
        put,
        get
      };
    }

    function budgetsOpened () {
      function put (budgetOpened) {
        return db.put(budgetOpened.toJSON()).then(res => {
          budgetOpened._rev = res.rev;
        });
      }

      function get (id) {
        return db.get(`${BudgetOpened.prefix}${id}`).then(b => {
          const budgetOpened = new BudgetOpened(b);
          budgetOpened.subscribe(put);

          return budgetOpened;
        });
      }

      function all () {
        return db.allDocs({
          include_docs: true,
          startkey: BudgetOpened.startKey,
          endkey: BudgetOpened.endKey
        }).then(res => {
          const budgetsOpened = {};

          for (let i = 0; i < res.rows.length; i++) {
            const budgetOpened = new BudgetOpened(res.rows[i].doc);
            budgetOpened.subscribe(put);

            budgetsOpened[budgetOpened.id] = budgetOpened;
          }

          return budgetsOpened;
        });
      }

      return {
        all,
        put,
        get
      };
    }
  };
}
