/*
╔══════════════════════════════════════════════════════════════════════════════╗
║ Financier MonthCategory Data Object                                          ║
╟──────────────────────────────────────────────────────────────────────────────╢
║ Author: Adam Romzek <asromzek@gmail.com>                                     ║
║ Description: Financier MonthCategory data object.                            ║
╚══════════════════════════════════════════════════════════════════════════════╝
*/

// ##################################################
//  Class wrapper function. Allows setting global
//  'budgetId' variable for static functions.
// ##################################################

export function MonthCategory () {
  //
  // ##################################################
  //  MonthCategory class definition.
  // ##################################################

  class MonthCategory {
    //
    // ==================================================
    //  Constructor.
    // ==================================================

    constructor (data) {
      //
      // --------------------------------------------------
      //  Check if data or _id is not set.
      // --------------------------------------------------

      if (!data || !data._id) {
        throw new Error(`
          Needs _id!
          To generate a new MonthCategory, use
          MonthCategory.from()
        `);
      }

      // --------------------------------------------------
      //  Initialize data object with default values if
      //  none are specified when the object is created.
      // --------------------------------------------------

      const myData = Object.assign({
        budget: 0,
        overspending: null,
        note: null
      }, data);

      // --------------------------------------------------
      //  Initialize the local data object.
      // --------------------------------------------------

      this._data = myData;

      // --------------------------------------------------
      //  Parse the category and month IDs.
      // --------------------------------------------------

      const s = myData._id.split('_');
      this.categoryId = s[s.length - 1];
      this.monthId = s[s.length - 2];
    }

    // ==================================================
    //  PouchDB JSON data object.
    // ==================================================

    get data () {
      return this._data;
    }

    set data (data) {
      const oldOverspending = this.data.overspending;
      this.data.overspending = data.overspending;

      if (this.data.overspending != null) {
        this._emitOverspendingChange(data.overspending, oldOverspending);
      }

      const oldBudget = this.data.budget;
      this.data.budget = data.budget;
      this._emitBudgetChange(data.budget - oldBudget);

      this.data.note = data.note;
      this.data._rev = data._rev;
    }

    // ==================================================
    //  Create a MonthCategory `_id` from other IDs.
    // ==================================================

    static _fromId (budgetId, monthId, categoryId) {
      return `${MonthCategory.prefix(budgetId)}${monthId}_${categoryId}`;
    }

    // ==================================================
    //  Create a MonthCategory `_id` from other IDs.
    // ==================================================

    static from (budgetId, monthId, categoryId) {
      return new MonthCategory({
        _id: MonthCategory._fromId(budgetId, monthId, categoryId)
      });
    }

    // ==================================================
    //  Budget value property.
    // ==================================================

    get budget () {
      return this.data.budget;
    }

    set budget (v) {
      const old = this.data.budget;
      if (v !== old) {
        this.data.budget = v;
        this._emitChange();
        this._emitBudgetChange(v - old);
      }
    }

    // ==================================================
    //  Note property.
    // ==================================================

    get note () {
      return this.data.note;
    }

    set note (n) {
      this.data.note = n;
      this._emitChange();
    }

    // ==================================================
    //  Overspending property.
    // ==================================================

    get overspending () {
      return this.data.overspending;
    }

    set overspending (o) {
      const oldO = this.data.overspending;
      this.data.overspending = o;
      this._emitOverspendingChange(o, oldO);
      this._emitChange();
    }

    // ==================================================
    //  PouchDB object ID.
    // ==================================================

    get _id () {
      return this.data._id;
    }

    // ==================================================
    //  Change subscription functions.
    // ==================================================

    subscribe (fn) {
      this.fn = fn;
    }

    subscribeBudget (fn) {
      this.budgetFn = fn;
    }

    subscribeOverspending (fn) {
      this.overspendingFn = fn;
    }

    // ==================================================
    //  Change event functions.
    // ==================================================

    _emitChange () {
      return this.fn && this.fn(this);
    }

    _emitOverspendingChange (newO, oldO) {
      return this.overspendingFn && this.overspendingFn(newO, oldO);
    }

    _emitBudgetChange (value) {
      return this.budgetFn && this.budgetFn(value);
    }

    // ==================================================
    //  Remove from the PouchDB database.
    // ==================================================

    remove () {
      this.data._deleted = true;
      return this._emitChange();
    }

    // ==================================================
    //  Return _data as javascript object.
    // ==================================================

    toJSON () {
      return this.data;
    }

    // ==================================================
    //  PouchDB object key components.
    // ==================================================

    static startKey (budgetId) {
      return `b_${budgetId}_m_category_`;
    }

    static endKey (budgetId) {
      return this.startKey(budgetId) + '\uffff';
    }

    static prefix (budgetId) {
      return this.startKey(budgetId);
    }

    static contains (budgetId, id) {
      return id > this.startKey(budgetId) && id < this.endKey(budgetId);
    }
  }

  // ==================================================
  //  Return the MonthCategory class.
  // ==================================================

  return MonthCategory;
}
