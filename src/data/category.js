/*
╔══════════════════════════════════════════════════════════════════════════════╗
║ Financier Category Data Object                                               ║
╟──────────────────────────────────────────────────────────────────────────────╢
║ Author: Adam Romzek <asromzek@gmail.com>                                     ║
║ Description: Financier Category data object.                                 ║
╚══════════════════════════════════════════════════════════════════════════════╝
*/

// ##################################################
//  Library imports.
// ##################################################

import uuid from 'uuid';

// ##################################################
//  Class wrapper function. Allows setting global
//  'budgetId' variable for static functions.
// ##################################################

export function Category (budgetId) {
  //
  // ##################################################
  //  Category class definition.
  // ##################################################

  class Category {
    //
    // ==================================================
    //  Constructor.
    // ==================================================

    constructor (data) {
      //
      // --------------------------------------------------
      //  Initialize data object with default values if
      //  none are specified when the object is created.
      // --------------------------------------------------

      const myData = Object.assign({
        name: 'New category',
        masterCategory: null,
        sort: 0,
        note: null,
        _id: Category.prefix + uuid.v4()
      }, data);

      // --------------------------------------------------
      //  Parse the Budget object ID.
      // --------------------------------------------------

      this.id = myData._id.slice(myData._id.lastIndexOf('_') + 1);

      // --------------------------------------------------
      //  Initialize the local data object.
      // --------------------------------------------------

      this._data = myData;

      // --------------------------------------------------
      //  Initial Financier Mobile pinned settings.
      // --------------------------------------------------

      const pinned = JSON.parse(localStorage.getItem('pinned_' + this.id));
      if (pinned === null) {
        this.fmobilePinned = false;
      } else {
        this.fmobilePinned = pinned;
      }

      const pinnedSort = JSON.parse(localStorage.getItem('pinned_' + this.id + '_sort'));
      if (pinnedSort === null) {
        this.fmobilePinnedSort = false;
      } else {
        this.fmobilePinnedSort = pinnedSort;
      }
    }

    // ==================================================
    //  PouchDB JSON data object.
    // ==================================================

    get data () {
      return this._data;
    }

    set data (d) {
      //
      // --------------------------------------------------
      //  fmobilePinned setting. Added for fmobile app by
      //  Adam Romzek, 12/16/2017.
      // --------------------------------------------------

      if (d.fmobilePinned !== undefined && this.fmobilePinned !== null) {
        this._data.fmobilePinned = d.fmobilePinned;
      } else {
        this._data.fmobilePinned = false;
      }

      // --------------------------------------------------
      //  fmobilePinnedSort setting. Added for fmobile app
      //  by Adam Romzek, 12/16/2017.
      // --------------------------------------------------

      if (d.fmobilePinnedSort !== undefined && this.fmobilePinnedSort !== null) {
        this._data.fmobilePinnedSort = d.fmobilePinnedSort;
      } else {
        this._data.fmobilePinnedSort = 0;
      }

      this._data.name = d.name;
      this._data.note = d.note;

      const oldSort = this._data.sort;
      this._data.sort = d.sort;

      const oldMasterCategory = this._data.masterCategory;

      if (oldMasterCategory !== d.masterCategory) {
        this.emitMasterCategoryChange(() => {
          this._data.masterCategory = d.masterCategory;
        });
      }

      if (oldSort !== d.sort) {
        this.emitSortChange();
      }

      this._data._rev = d._rev;
    }

    // ==================================================
    //  Name of the category.
    // ==================================================

    get name () {
      return this._data.name;
    }

    set name (n) {
      this._data.name = n;
      this.emitChange();
    }

    // ==================================================
    //  ID of the master category that the category
    //  belongs to.
    // ==================================================

    get masterCategory () {
      return this._data.masterCategory;
    }

    set masterCategory (n) {
      if (this._data.masterCategory !== n) {
        this._data.masterCategory = n;
        this.emitChange();
      }
    }

    // ==================================================
    //  Set or change the master category that the
    //  category belongs to, and set the sort index.
    // ==================================================

    setMasterAndSort (masterCategory, i) {
      const saveFn = this.fn;
      this.fn = null;

      const oldSort = this.sort;
      const master = this.masterCategory;

      this.masterCategory = masterCategory;
      this.sort = i;

      this.fn = saveFn;

      if (i !== oldSort || masterCategory !== master) {
        this.emitChange();
      }
    }

    // ==================================================
    //  Category list sort index.
    // ==================================================

    get sort () {
      return this._data.sort;
    }

    set sort (i) {
      if (this._data.sort !== i) {
        this._data.sort = i;
        this.emitChange();
      }
    }

    // ==================================================
    //  Category note.
    // ==================================================

    get note () {
      return this._data.note;
    }

    set note (n) {
      this._data.note = n;
      this.emitChange();
    }

    // ==================================================
    //  Fmobile category pin-to-top.
    // ==================================================

    get fmobilePinned () {
      return this._fmobilePinned;
    }

    set fmobilePinned (value) {
      this._fmobilePinned = value;
      localStorage.setItem('pinned_' + this.id, JSON.stringify(value));
    }

    // ==================================================
    //  Fmobile category sort index in pinned list.
    // ==================================================

    get fmobilePinnedSort () {
      return this._fmobilePinnedSort;
    }

    set fmobilePinnedSort (value) {
      this._fmobilePinnedSort = value;
      localStorage.setItem('pinned_' + this.id + '_sort', JSON.stringify(value));
    }

    // ==================================================
    //  PouchDB object ID.
    // ==================================================

    get _id () {
      return this._data._id;
    }

    // ==================================================
    //  Change subscription functions.
    // ==================================================

    subscribe (fn) {
      this.fn = fn;
    }

    subscribeSortChange (fn) {
      this.sortFn = fn;
    }

    subscribeMasterCategoryChange (before, after) {
      this.masterCategoryBeforeFn = before;
      this.masterCategoryAfterFn = after;
    }

    // ==================================================
    //  Change event functions.
    // ==================================================

    emitChange () {
      return this.fn && this.fn(this);
    }

    emitSortChange () {
      return this.sortFn && this.sortFn(this);
    }

    emitMasterCategoryChange (fn) {
      this.masterCategoryBeforeFn(this);
      fn();
      this.masterCategoryAfterFn(this);
    }

    // ==================================================
    //  Remove from the PouchDB database.
    // ==================================================

    remove () {
      this._data._deleted = true;
      return this.emitChange();
    }

    // ==================================================
    //  Return _data as javascript object.
    // ==================================================

    toJSON () {
      return this._data;
    }

    // ==================================================
    //  PouchDB object key components.
    // ==================================================

    static get startKey () {
      return `b_${budgetId}_category_`;
    }

    static get endKey () {
      return this.startKey + '\uffff';
    }

    static get prefix () {
      return this.startKey;
    }

    static contains (id) {
      return id > this.startKey && id < this.endKey;
    }
  }

  // ==================================================
  //  Return the Category class.
  // ==================================================

  return Category;
}
