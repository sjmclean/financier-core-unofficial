/*
╔══════════════════════════════════════════════════════════════════════════════╗
║ Financier Month Data Object                                                  ║
╟──────────────────────────────────────────────────────────────────────────────╢
║ Author: Adam Romzek <asromzek@gmail.com>                                     ║
║ Description: Financier Month data object.                                    ║
╚══════════════════════════════════════════════════════════════════════════════╝
*/

// ##################################################
//  Project data imports.
// ##################################################

import { MonthCategory as monthCategory } from './month-category';

// ##################################################
//  Class wrapper function. Allows setting global
//  'budgetId' variable for static functions.
// ##################################################

export function Month (budgetId) {
  //
  // ==================================================
  //  Initialize data object classes.
  // ==================================================

  const MonthCategory = monthCategory();
  //
  // ##################################################
  //  Month class definition.
  // ##################################################

  class Month {
    //
    // ==================================================
    //  Constructor.
    // ==================================================

    constructor (data, saveFn) {
      //
      // --------------------------------------------------
      //  Initialize defaults.
      // --------------------------------------------------

      const defaults = {
      };

      // --------------------------------------------------
      //  Set budgetId and saveFn variables.
      // --------------------------------------------------

      this.saveFn = saveFn;

      // --------------------------------------------------
      //  Initialize local data variable.
      // --------------------------------------------------

      let myData;

      // --------------------------------------------------
      //  Check incoming data and extend if necessary.
      // --------------------------------------------------

      if (typeof data === 'string') {
        myData = defaults;
        myData._id = `b_${budgetId}_month_${data}`;
      } else {
        myData = Object.assign(defaults, data);
      }

      // --------------------------------------------------
      //  Initialize class variables.
      // --------------------------------------------------

      this.date = myData._id.slice(myData._id.lastIndexOf('_') + 1);
      this.data = myData;
      this.categories = {};
      this.categoryCache = {};
      this.cache = {
        totalTransactions: 0,
        totalBudget: 0,
        totalBalance: 0,
        totalAvailable: 0,
        totalAvailableLastMonth: 0,
        totalOverspent: 0,
        totalOverspentLastMonth: 0,
        totalIncome: 0, // todo
        totalOutflow: 0
      };
    }

    // ==================================================
    //  Call with the overflowed (rolling) balance from
    //  last month's catId.
    // ==================================================

    setRolling (catId, rolling, overspending) {
      this.createCategoryIfEmpty(catId);
      this.createCategoryCacheIfEmpty(catId);

      if (this.categoryCache[catId].overspending == null) {
        if (overspending) {
          this._changeCurrentOverspent(Math.min(this.categoryCache[catId].balance, 0));
        }

        this.categoryCache[catId].overspending = overspending;
      }

      const oldRolling = this.categoryCache[catId].rolling;
      this.categoryCache[catId].rolling = rolling;

      const oldBalance = this.categoryCache[catId].balance;
      this.categoryCache[catId].balance += rolling - oldRolling;

      this.cache.totalBalance += rolling - oldRolling;

      if (this.categoryCache[catId].overspending) {
        this.nextRollingFn && this.nextRollingFn(catId, this.categoryCache[catId].balance, this.categoryCache[catId].overspending);
      } else {
        this._changeCurrentOverspent(0 - (Math.min(this.categoryCache[catId].balance, 0) - Math.min(oldBalance, 0)));

        if (this.categoryCache[catId].balance > 0) {
          this.nextRollingFn && this.nextRollingFn(catId, this.categoryCache[catId].balance, this.categoryCache[catId].overspending);
        } else {
          this.nextRollingFn && this.nextRollingFn(catId, 0, this.categoryCache[catId].overspending);
        }
      }
    }

    // ==================================================
    //  Should be called by the MonthManager on the very
    //  first month upon initialization to propagate the
    //  rolling balances.
    // ==================================================

    startRolling (catId) {
      this.createCategoryCacheIfEmpty(catId);
      this.setRolling(catId, this.categoryCache[catId].rolling, this.categoryCache[catId].overspending);
    }

    // ==================================================
    //  Set a budget amount for a category, whether it
    //  exists or not.
    // ==================================================

    setBudget (catId, amount) {
      this.createCategoryCacheIfEmpty(catId);
      this.createCategoryIfEmpty(catId);

      this.categories[catId].budget = amount;
    }

    // ==================================================
    //  Add existing MonthCategory object to the month.
    // ==================================================

    addBudget (monthCat) {
      // Assume is MonthCategory.
      this.categories[monthCat.categoryId] = monthCat;
      this.createCategoryCacheIfEmpty(monthCat.categoryId);

      monthCat.subscribe(record => {
        this.saveFn(record);
      });

      monthCat.subscribeBudget(value => {
        this.budgetChange(monthCat.categoryId, value);
      });

      monthCat.subscribeOverspending((newO, oldO) => {
        const oldOverspending = this.categoryCache[monthCat.categoryId].overspending;
        this.categoryCache[monthCat.categoryId].overspending = newO;

        // We need to manually restore the overspent amount
        // to whatever the functionality should be (for this category)
        if (newO && !oldOverspending) {
          this._changeCurrentOverspent(Math.min(0, this.categoryCache[monthCat.categoryId].balance));
        } else if (!newO && oldOverspending) {
          this._changeCurrentOverspent(-Math.min(0, this.categoryCache[monthCat.categoryId].balance));
        }

        this.startRolling(monthCat.categoryId);
      });

      if (monthCat.overspending !== null) {
        this.categoryCache[monthCat.categoryId].overspending = monthCat.overspending;
      }

      this.cache.totalBudget += monthCat.budget;

      this.changeAvailable(-monthCat.budget);

      this.categoryCache[monthCat.categoryId].balance += monthCat.budget;

      if (!this.categoryCache[monthCat.categoryId].overspending) {
        this._changeCurrentOverspent(-Math.min(0, monthCat.budget));
      }

      this.cache.totalBalance += monthCat.budget;

      // Don't call nextRollingFn quite yet since we're adding the MonthCategory
      // upon initialization, and we'll run startRolling on the whole collection
      // of months later
    }

    // ==================================================
    //  Add existing MonthCategory object to the month.
    // ==================================================

    removeBudget (monthCat) {
      // Assume is MonthCategory.
      monthCat.subscribeBudget(null);

      this.cache.totalBudget -= monthCat.budget;

      this.changeAvailable(monthCat.budget);

      if (this.categoryCache[monthCat.categoryId]) {
        this.categoryCache[monthCat.categoryId].balance -= monthCat.budget;

        if (!this.categoryCache[monthCat.categoryId].overspending) {
          this._changeCurrentOverspent(Math.min(0, monthCat.budget));
        }
      }

      this.cache.totalBalance -= monthCat.budget;

      delete this.categories[monthCat.categoryId];

      // Don't call nextRollingFn quite yet since we're removing the MonthCategory
      // upon initialization, and we'll run startRolling on the whole collection
      // of months later
    }

    // ==================================================
    //  Add transaction.
    // ==================================================

    addTransaction (trans) {
      // The splits themselves are added, not the parent
      if (trans.category === 'split') {
        return;
      }

      if (trans.category === 'income' || trans.category === 'incomeNextMonth') {
        this._addIncome(trans);
      } else {
        this._addOutflow(trans);
      }
    }

    // ==================================================
    //  Add income to transaction.
    // ==================================================

    _addIncome (trans) {
      const valueChangeFn = val => {
        this.cache.totalIncome += val;
        this.changeAvailable(val);
      };

      valueChangeFn(trans.value);

      trans.subscribeValueChange(valueChangeFn);
    }

    // ==================================================
    //  Add outflow to transaction.
    // ==================================================

    _addOutflow (trans) {
      this.createCategoryCacheIfEmpty(trans.category);

      const valueChangeFn = val => {
        this.categoryCache[trans.category].outflow += val;
        this.cache.totalOutflow += val;

        const oldBalance = this.categoryCache[trans.category].balance;
        this.categoryCache[trans.category].balance += val;

        if (!this.categoryCache[trans.category].overspending) {
          this._changeCurrentOverspent(0 - (Math.min(this.categoryCache[trans.category].balance, 0) - Math.min(oldBalance, 0)));
        }

        this.cache.totalBalance += val;

        this.startRolling(trans.category); // todo optimize
      };

      valueChangeFn(trans.value);
      trans.subscribeValueChange(valueChangeFn);
    }

    // ==================================================
    //  Remove transaction.
    // ==================================================

    removeTransaction (trans) {
      // The splits themselves are added, not the parent
      if (trans.category === 'split') {
        return;
      }

      if (trans.category === 'income' || trans.category === 'incomeNextMonth') {
        this._removeIncome(trans);
      } else {
        this._removeOutflow(trans);
      }
    }

    // ==================================================
    //  Remove income from transaction.
    // ==================================================

    _removeIncome (trans) {
      this.cache.totalIncome -= trans.value;
      this.changeAvailable(-trans.value);

      trans.subscribeValueChange(null);
    }

    // ==================================================
    //  Remove outflow from transaction.
    // ==================================================

    _removeOutflow (trans) {
      this.createCategoryCacheIfEmpty(trans.category);

      this.categoryCache[trans.category].outflow -= trans.value;
      this.cache.totalOutflow -= trans.value;

      const oldBalance = this.categoryCache[trans.category].balance;
      this.categoryCache[trans.category].balance -= trans.value;

      if (!this.categoryCache[trans.category].overspending) {
        this._changeCurrentOverspent(0 - (Math.min(this.categoryCache[trans.category].balance, 0) - Math.min(oldBalance, 0)));
      }

      this.cache.totalBalance -= trans.value;
      this.startRolling(trans.category); // TODO: Optimize startRolling(trans.category)
      trans.subscribeValueChange(null);
    }

    // ==================================================
    //  Provides a way to set a note on a category.
    // ==================================================

    note (catId) {
      this.createCategoryIfEmpty(catId);

      return note => {
        if (typeof note !== 'undefined') {
          this.categories[catId].note = note;
        } else {
          return this.categories[catId].note;
        }
      };
    }

    // ==================================================
    //  Used to create and set up a MonthCategory if one
    //  doesn't already exist from addBudget.
    // ==================================================

    createCategoryIfEmpty (catId) {
      this.createCategoryCacheIfEmpty(catId);

      if (!this.categories[catId]) {
        /* eslint-disable new-cap */
        this.categories[catId] = MonthCategory.from(budgetId, this.date, catId);

        this.categories[catId].subscribe(record => {
          this.saveFn(record);
        });

        this.categories[catId].subscribeBudget(value => {
          this.budgetChange(catId, value);
        });

        this.categories[catId].subscribeOverspending((newO, oldO) => {
          const oldOverspending = this.categoryCache[catId].overspending;
          this.categoryCache[catId].overspending = newO;

          // We need to manually restore the overspent amount
          // to whatever the functionality should be (for this category)
          if (newO && !oldOverspending) {
            this._changeCurrentOverspent(Math.min(0, this.categoryCache[catId].balance));
          } else if (!newO && oldOverspending) {
            this._changeCurrentOverspent(-Math.min(0, this.categoryCache[catId].balance));
          }

          this.startRolling(catId);
        });
      }
    }

    // ==================================================
    //  Used to create a place to hold the rolling amount
    //  and current balance for the given category, if
    //  one doesn't already exist.
    // ==================================================

    createCategoryCacheIfEmpty (catId) {
      if (!this.categoryCache[catId]) {
        this.categoryCache[catId] = {
          rolling: 0,
          outflow: 0,
          balance: 0,
          overspending: null
        };
      }
    }

    // ==================================================
    //  Create a Month date ID (minus the ID prefix).
    //  Will always be the 1st of the month.
    // ==================================================

    static createID (date) {
      const twoDigitMonth = ((date.getMonth() + 1) >= 10) ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1);
      return date.getFullYear() + '-' + twoDigitMonth + '-01';
    }

    // ==================================================
    //  Usually called by MonthCategories when they have
    //  a budget amount change.
    // ==================================================

    budgetChange (catId, value) {
      this.cache.totalBudget += value;

      this.changeAvailable(-value);

      const oldBalance = this.categoryCache[catId].balance;
      this.categoryCache[catId].balance += value;

      this.cache.totalBalance += value;

      if (this.categoryCache[catId].overspending) {
        this.nextRollingFn && this.nextRollingFn(catId, this.categoryCache[catId].balance);
      } else {
        this._changeCurrentOverspent(0 - (Math.min(this.categoryCache[catId].balance, 0) - Math.min(oldBalance, 0)));

        if (this.categoryCache[catId].balance > 0) {
          this.nextRollingFn && this.nextRollingFn(catId, this.categoryCache[catId].balance);
        } else {
          // Your category balance is overdrawn!
          this.nextRollingFn && this.nextRollingFn(catId, 0);
        }
      }
    }

    // ==================================================
    //  Change subscription functions.
    // ==================================================

    subscribeNextMonth (nextMonth) {
      this.nextRollingFn = (catId, balance, overspending) => {
        nextMonth.setRolling(catId, balance, overspending);
      };
      this.nextChangeAvailableFn = val => {
        nextMonth.changeAvailable(val);
      };
      this.nextChangeOverspentFn = val => {
        nextMonth.changeOverspent(val);
      };
      this.nextChangeAvailableLastMonthFn = val => {
        nextMonth.changeAvailableLastMonth(val);
      };

      // Initialize totalAvailable of next month.
      this.nextChangeAvailableFn && this.nextChangeAvailableFn(this.cache.totalAvailable);

      // Initialize totalAvailableLastMonth of next month.
      this.nextChangeAvailableLastMonthFn && this.nextChangeAvailableLastMonthFn(this.cache.totalAvailable);

      // Initialize totalOverspent of next month.
      this.nextChangeOverspentFn && this.nextChangeOverspentFn(this.cache.totalOverspent);
    }

    // ==================================================
    //  Change event functions.
    // ==================================================

    changeAvailable (value) {
      this.cache.totalAvailable += value;
      this.nextChangeAvailableLastMonthFn && this.nextChangeAvailableLastMonthFn(value);
      this.nextChangeAvailableFn && this.nextChangeAvailableFn(value);
    }

    changeAvailableLastMonth (value) {
      this.cache.totalAvailableLastMonth += value;
    }

    _changeCurrentOverspent (value) {
      this.cache.totalOverspent += value;
      this.nextChangeOverspentFn && this.nextChangeOverspentFn(value);
    }

    changeOverspent (value) {
      this.cache.totalOverspentLastMonth += value;
      this.changeAvailable(-value);
    }

    // ==================================================
    //  Return data as javascript object.
    // ==================================================

    toJSON () {
      return this.data;
    }

    // ==================================================
    //  PouchDB object key components.
    // ==================================================

    static get startKey () {
      return `b_${budgetId}_month_`;
    }

    static get endKey () {
      return this.startKey + '\uffff';
    }

    static get prefix () {
      return this.startKey;
    }

    static contains (id) {
      return id > this.startKey && id < this.endKey;
    }
  }

  // ==================================================
  //  Return the Month class.
  // ==================================================

  return Month;
}
