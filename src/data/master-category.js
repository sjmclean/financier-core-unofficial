/*
╔══════════════════════════════════════════════════════════════════════════════╗
║ Financier MasterCategory Data Object                                         ║
╟──────────────────────────────────────────────────────────────────────────────╢
║ Author: Adam Romzek <asromzek@gmail.com>                                     ║
║ Description: Financier MasterCategory data object.                           ║
╚══════════════════════════════════════════════════════════════════════════════╝
*/

// ##################################################
//  Library imports.
// ##################################################

import uuid from 'uuid';

// ##################################################
//  Class wrapper function. Allows setting global
//  'budgetId' variable for static functions.
// ##################################################

export function MasterCategory (budgetId) {
  //
  // ##################################################
  //  MasterCategory class definition.
  // ##################################################

  class MasterCategory {
    //
    // ==================================================
    //  Constructor.
    // ==================================================

    constructor (data) {
      //
      // --------------------------------------------------
      //  Initialize data object with default values if
      //  none are specified when the object is created.
      // --------------------------------------------------

      const myData = Object.assign({
        name: 'New master category',
        _id: MasterCategory.prefix + uuid.v4(),
        sort: 0,
        collapsed: false
      }, data);

      // --------------------------------------------------
      //  Parse the Budget object ID.
      // --------------------------------------------------

      this.id = myData._id.slice(myData._id.lastIndexOf('_') + 1);

      // --------------------------------------------------
      //  Initialize the local data object.
      // --------------------------------------------------

      this._data = myData;

      // --------------------------------------------------
      //  Initialize the categories array.
      // --------------------------------------------------

      this._categories = [];
      this._categories.masterCategory = this;
    }

    // ==================================================
    //  PouchDB JSON data object.
    // ==================================================

    get data () {
      return this._data;
    }

    set data (d) {
      this._data.sort = d.sort;
      this._data.name = d.name;
      this._data.collapsed = d.collapsed;
      this._data._rev = d._rev;
    }

    // ==================================================
    //  Name of the master category.
    // ==================================================

    get name () {
      return this._data.name;
    }

    set name (n) {
      this._data.name = n;
      this.emitChange();
    }

    // ==================================================
    //  Master category collapsed memory.
    // ==================================================

    get collapsed () {
      return this._data.collapsed;
    }

    set collapsed (n) {
      this._data.collapsed = n;
      this.emitChange();
    }

    // ==================================================
    //  Category array contained within this master
    //  category.
    // ==================================================

    get categories () {
      return this._categories;
    }

    set categories (arr) {
      arr.masterCategory = this;
      this._categories = arr;
    }

    // ==================================================
    //  Add a category to this master category.
    // ==================================================

    addCategory (cat) {
      this._categories.push(cat);

      const sort = () => {
        this._categories.sort((a, b) => a.sort - b.sort);
      };

      sort();

      cat.subscribeSortChange(sort);
    }

    // ==================================================
    //  Remove a category from this master category.
    // ==================================================

    removeCategory (cat) {
      const index = this._categories.indexOf(cat);

      if (index !== -1) {
        this._categories.splice(index, 1);
      }
    }

    // ==================================================
    //  Master category list sort index.
    // ==================================================

    get sort () {
      return this._data.sort;
    }

    set sort (i) {
      if (this._data.sort !== i) {
        this._data.sort = i;
        this.emitChange();
      }
    }

    // ==================================================
    //  PouchDB object ID.
    // ==================================================

    get _id () {
      return this._data._id;
    }

    // ==================================================
    //  Change subscription function.
    // ==================================================

    subscribe (fn) {
      this.fn = fn;
    }

    // ==================================================
    //  Change event function.
    // ==================================================

    emitChange () {
      this.fn && this.fn(this);
    }

    // ==================================================
    //  Remove from the PouchDB database.
    // ==================================================

    remove () {
      this._data._deleted = true;
      this.emitChange();
    }

    // ==================================================
    //  Return _data as javascript object.
    // ==================================================

    toJSON () {
      return this._data;
    }

    // ==================================================
    //  PouchDB object key components.
    // ==================================================

    static get startKey () {
      return `b_${budgetId}_master-category_`;
    }

    static get endKey () {
      return this.startKey + '\uffff';
    }

    static get prefix () {
      return this.startKey;
    }

    static contains (id) {
      return id > this.startKey && id < this.endKey;
    }
  }

  // ==================================================
  //  Return the MasterCategory class.
  // ==================================================

  return MasterCategory;
}
