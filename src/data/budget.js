/*
╔══════════════════════════════════════════════════════════════════════════════╗
║ Financier Budget Data Object                                                 ║
╟──────────────────────────────────────────────────────────────────────────────╢
║ Author: Adam Romzek <asromzek@gmail.com>                                     ║
║ Description: Financier Budget data object.                                   ║
╚══════════════════════════════════════════════════════════════════════════════╝
*/

// ##################################################
//  Library imports.
// ##################################################

import uuid from 'uuid';

// ##################################################
//  Class wrapper function.
// ##################################################

export function Budget () {
//
// ##################################################
//  Budget class definition.
// ##################################################

  class Budget {
    //
    // ==================================================
    //  Constructor.
    // ==================================================

    constructor (data) {
      //
      // --------------------------------------------------
      //  Initialize data object with default values if
      //  none are specified when the object is created.
      // --------------------------------------------------

      this._data = Object.assign({
        hints: {
          outflow: true
        },
        name: null,
        currency: 'USD',
        _id: 'budget_' + uuid.v4(),
        created: new Date().toISOString(),
        checkNumber: false
      }, data);

      // --------------------------------------------------
      //  Initialize hints.
      // --------------------------------------------------

      const that = this;
      this._hints = {
        get outflow () {
          return that.data.hints.outflow;
        },
        set outflow (o) {
          that.data.hints.outflow = o;
          that.emitChange();
        }
      };

      // --------------------------------------------------
      //  Parse the Budget object ID.
      // --------------------------------------------------

      this.id = this._data._id.slice(this._data._id.lastIndexOf('_') + 1);
    }

    // ==================================================
    //  PouchDB JSON data object.
    // ==================================================

    get data () {
      return this._data;
    }

    set data (data) {
      this._data = data;
    }

    // ==================================================
    //  Name of the budget.
    // ==================================================

    get name () {
      return this._data.name;
    }

    set name (n) {
      this._data.name = n;
      this.emitChange();
    }

    // ==================================================
    //  Hint, hint.
    // ==================================================

    get hints () {
      return this._hints;
    }

    // ==================================================
    //  Budget currency code.
    // ==================================================

    get currency () {
      return this._data.currency;
    }

    set currency (c) {
      this._data.currency = c;
      this.emitChange();
    }

    // ==================================================
    //  Show check number in accounts by default.
    // ==================================================

    get checkNumber () {
      return this._data.checkNumber;
    }

    set checkNumber (c) {
      this._data.checkNumber = c;
      this.emitChange();
    }

    // ==================================================
    //  Date that the budget was created.
    // ==================================================

    get created () {
      return new Date(this._data.created);
    }

    // ==================================================
    //  PouchDB object ID.
    // ==================================================

    get _id () {
      return this._data._id;
    }

    // ==================================================
    //  PouchDB object revision.
    // ==================================================

    set _rev (r) {
      this._data._rev = r;
    }

    // ==================================================
    //  Change subscription function.
    // ==================================================

    subscribe (fn) {
      this.fn = fn;
    }

    // ==================================================
    //  Change event function.
    // ==================================================

    emitChange () {
      return this.fn && this.fn(this);
    }

    // ==================================================
    //  Remove from the PouchDB database.
    // ==================================================

    remove () {
      this._data._deleted = true;
      return this.emitChange();
    }

    // ==================================================
    //  Return _data as javascript object.
    // ==================================================

    toJSON () {
      return this._data;
    }

    // ==================================================
    //  PouchDB object key components.
    // ==================================================

    static get startKey () {
      return 'budget_';
    }

    static get endKey () {
      return this.startKey + '\uffff';
    }

    static get prefix () {
      return this.startKey;
    }

    static contains (id) {
      return id > this.startKey && id < this.endKey;
    }
  }

  // ==================================================
  //  Return the Budget class.
  // ==================================================

  return Budget;
}
