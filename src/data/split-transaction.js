/*
╔══════════════════════════════════════════════════════════════════════════════╗
║ Financier SplitTransaction Data Object                                       ║
╟──────────────────────────────────────────────────────────────────────────────╢
║ Author: Adam Romzek <asromzek@gmail.com>                                     ║
║ Description: Financier SplitTransaction data object.                         ║
╚══════════════════════════════════════════════════════════════════════════════╝
*/

// ##################################################
//  Library imports.
// ##################################################

import moment from 'moment';
import uuid from 'uuid';

// ##################################################
//  Class wrapper function.
// ##################################################

export function SplitTransaction () {
  //
  // ##################################################
  //  SplitTransaction class definition.
  // ##################################################

  class SplitTransaction {
    //
    // ==================================================
    //  Constructor.
    // ==================================================

    constructor (transaction, data) {
      //
      // --------------------------------------------------
      //  Initialize data object with default values if
      //  none are specified when the object is created.
      // --------------------------------------------------

      const myData = Object.assign({
        id: uuid.v4(),
        value: 0,
        category: null,
        memo: null,
        payee: null,
        transfer: null
      }, data);

      // --------------------------------------------------
      //  Initialize class variables.
      // --------------------------------------------------

      this.transaction = transaction;
      this.transfer = null;
      this.id = myData.id;

      // --------------------------------------------------
      //  Ensure whole number for the value.
      // --------------------------------------------------

      myData.value = Math.round(myData.value);

      // --------------------------------------------------
      //  Initialize the local data object.
      // --------------------------------------------------

      this._data = myData;

      // --------------------------------------------------
      //  Set the month for the split.
      // --------------------------------------------------

      this.setMonth();

      // --------------------------------------------------
      //  Initialize subscription arrays.
      // --------------------------------------------------

      this.subscribeClearedValueChangeFn = [];
      this.subscribeUnclearedValueChangeFn = [];
    }

    // ==================================================
    //  Object type description.
    // ==================================================

    get constructorName () {
      return 'SplitTransaction';
    }

    // ==================================================
    //  PouchDB JSON data object.
    // ==================================================

    get data () {
      return this._data;
    }

    set data (data) {
      // SET CATEGORY
      this._emitCategoryChange(() => {
        this._data.category = data.category;
        this.setMonth();
      });

      this._emitValueChange(data.value - this._data.value);
      this._data = data;
    }

    // ==================================================
    //  Remove split from the parent transaction, or not.
    // ==================================================

    _remove () {
      // this.transaction.removeSplit(this);
    }

    // ==================================================
    //  Value of the split.
    // ==================================================

    get value () {
      return this._data.value;
    }

    set value (x) {
      const oldValue = this._data.value;
      this._data.value = x;

      this._emitValueChange(x - oldValue);
      this._emitChange();

      if (this.transfer) {
        const transOldValue = this.transfer._data.value;
        this.transfer._data.value = -x;

        if (this.transfer.cleared) {
          this.transfer._emitClearedValueChange(-x - transOldValue);
        } else {
          this.transfer._emitUnclearedValueChange(-x - transOldValue);
        }

        this.transfer._emitValueChange(-x - transOldValue);
        this.transfer._emitChange();
      }
    }

    // ==================================================
    //  Pass through to the date of the parent
    //  transaction.
    // ==================================================

    get date () {
      return this.transaction.date;
    }

    set date (d) {
      this.transaction.date = d;
    }

    // ==================================================
    //  Transaction month.
    // ==================================================

    get month () {
      return this._month;
    }

    setMonth () {
      this._month = moment(this.date);

      if (this.category === 'incomeNextMonth') {
        this._month = this._month.add(1, 'month');
      }

      this._month = this._month.toDate();
    }

    // ==================================================
    //  Transaction on the other end if this is split is
    //  a transfer.
    // ==================================================

    get transfer () {
      return this._transfer;
    }

    set transfer (transfer) {
      this._transfer = transfer;
    }

    // ==================================================
    //  Payee for the split.
    // ==================================================

    get payee () {
      return this._data.payee;
    }

    set payee (payee) {
      if (payee !== this._data.payee) {
        this._data.payee = payee;
        this._emitChange();
      }
    }

    // ==================================================
    //  Value converted to a positive outlfow if
    //  negative.
    // ==================================================

    get outflow () {
      if (this.value < 0) {
        return Math.abs(this.value);
      }
    }

    set outflow (v) {
      this.value = -v;
    }

    // ==================================================
    //  Value converted to a positive inflow if positive.
    // ==================================================

    get inflow () {
      if (this.value > 0) {
        return this.value;
      }
    }

    set inflow (v) {
      this.value = v;
    }

    // ==================================================
    //  The category the split is assigned to.
    // ==================================================

    get category () {
      return this._data.category;
    }

    set category (x) {
      this._emitCategoryChange(() => {
        this._data.category = x;
        this.setMonth();
      });

      this._emitChange();
    }

    // ==================================================
    //  The account that the parent transaction is
    //  assigned to.
    // ==================================================

    get account () {
      return this.transaction.account;
    }

    set account (s) {
      // You should never be able to set the account of a split transaction...
      // So let's just do nothing
      // return
    }

    // ==================================================
    //  Memo for the split.
    // ==================================================

    get memo () {
      return this._data.memo;
    }

    set memo (x) {
      this._data.memo = x;

      if (this.transfer) {
        this.transfer.data.memo = x;
        this.transfer._emitChange();
      }

      this._emitChange();
    }

    // ==================================================
    //  Set date of parent transaction when editing split.
    // ==================================================

    _setDate (x) {
      const splitOldMonth = this.month;
      this.transaction._setDate(x);
      this.setMonth();
      this._emitMonthChange(this.month, splitOldMonth);
    }

    // ==================================================
    //  Set date when editing parent transaction.
    // ==================================================

    _setDateFromParent (x) {
      const oldMonth = this._month;
      this.setMonth();
      this._emitMonthChange(this.month, oldMonth);

      if (this.transfer) {
        this.transfer._setDate(x);
      }
    }

    // ==================================================
    //  Subscription function.
    // ==================================================

    set fn (f) {
      this.transaction.fn = f;
    }

    get fn () {
      return this.transaction.fn;
    }

    // ==================================================
    //  Change subscription functions.
    // ==================================================

    subscribe (fn) {
      this.fn = fn;
    }

    subscribeMonthChange (fn) {
      this.subscribeMonthChangeFn = fn;
    }

    subscribeCategoryChange (beforeFn, afterFn) {
      this.subscribeCategoryChangeBeforeFn = beforeFn;
      this.subscribeCategoryChangeAfterFn = afterFn;
    }

    subscribeValueChange (fn) {
      this.subscribeValueChangeFn = fn;
    }

    subscribePayeeChange (fn) {
      this.subscribePayeeChangeFn = fn;
    }

    subscribeAccountChange () {
      // Do nothing -- no-op for Transaction vs SplitTransaction
      // (SplitTransactions do not care if the account changes)
    }

    // ==================================================
    //  Change event functions.
    // ==================================================

    _emitChange () {
      return this.fn && this.fn(this.transaction);
    }

    _emitMonthChange (newMonth, oldMonth) {
      this.subscribeMonthChangeFn && this.subscribeMonthChangeFn(newMonth, oldMonth);
    }

    _emitCategoryChange (fn) {
      this.subscribeCategoryChangeBeforeFn && this.subscribeCategoryChangeBeforeFn();
      fn();
      this.subscribeCategoryChangeAfterFn && this.subscribeCategoryChangeAfterFn();
    }

    _emitPayeeChange (newPayee, oldPayee) {
      return this.subscribePayeeChangeFn && this.subscribePayeeChangeFn(newPayee, oldPayee);
    }

    _emitValueChange (val) {
      return this.subscribeValueChangeFn && this.subscribeValueChangeFn(val);
    }

    _emitClearedValueChange (val) {
      for (let i = 0; i < this.subscribeClearedValueChangeFn.length; i++) {
        this.subscribeClearedValueChangeFn[i](val);
      }
    }

    _emitUnclearedValueChange (val) {
      for (let i = 0; i < this.subscribeUnclearedValueChangeFn.length; i++) {
        this.subscribeUnclearedValueChangeFn[i](val);
      }
    }

    // ==================================================
    //  Remove from the PouchDB database.
    // ==================================================

    remove () {
      if (this.transfer && !this.transfer._data._deleted) {
        this.transfer._data._deleted = true;
        this.transfer._emitChange();
      }

      // if (!this._data._deleted) {
      //   this._data._deleted = true;
      //   this._emitChange();
      // }
    }

    // ==================================================
    //  Return _data as javascript object.
    // ==================================================

    toJSON () {
      return this._data;
    }
  }

  // ==================================================
  //  Return the SplitTransaction class.
  // ==================================================

  return SplitTransaction;
}
