/*
╔══════════════════════════════════════════════════════════════════════════════╗
║ Financier Transaction Data Object                                            ║
╟──────────────────────────────────────────────────────────────────────────────╢
║ Author: Adam Romzek <asromzek@gmail.com>                                     ║
║ Description: Financier Transaction data object.                              ║
╚══════════════════════════════════════════════════════════════════════════════╝
*/

// ##################################################
//  Library imports.
// ##################################################

import moment from 'moment';
import uuid from 'uuid';

// ##################################################
//  Project imports.
// ##################################################

import { SplitTransaction as splitTransaction } from './split-transaction';

// ##################################################
//  Class wrapper function. Allows setting global
//  'budgetId' variable for static functions.
// ##################################################

export function Transaction (budgetId) {
  //
  // ==================================================
  //  Initialize data object classes.
  // ==================================================

  const SplitTransaction = splitTransaction(budgetId);

  //
  // ##################################################
  //  Transaction class definition.
  // ##################################################

  class Transaction {
    //
    // ==================================================
    //  Constructor.
    // ==================================================

    constructor (data) {
      //
      // --------------------------------------------------
      //  Initialize data object with default values if
      //  none are specified when the object is created.
      // --------------------------------------------------

      const myData = Object.assign({
        _id: Transaction.prefix + uuid.v4(),
        value: 0,
        date: null,
        category: null,
        account: null,
        memo: null,
        cleared: false,
        reconciled: false,
        flag: null,
        payee: null,
        transfer: null,
        splits: [],
        checkNumber: null
      }, data);

      // --------------------------------------------------
      //  Initialize class variables.
      // --------------------------------------------------

      this.transfer = null;

      // --------------------------------------------------
      //  Ensure whole number for the value.
      // --------------------------------------------------

      myData.value = Math.round(myData.value);

      // --------------------------------------------------
      //  Parse the Transaction object ID.
      // --------------------------------------------------

      this.id = myData._id.slice(myData._id.lastIndexOf('_') + 1);

      if (myData.date) {
        if (myData.date.length !== 10) {
          myData.date = myData.date.substring(0, 10);
        }

        this._date = moment(myData.date).toDate();
      }

      // --------------------------------------------------
      //  Initialize the local data object.
      // --------------------------------------------------

      this._data = myData;

      // --------------------------------------------------
      //  Set the month for the split.
      // --------------------------------------------------

      this.setMonth();

      // --------------------------------------------------
      //  Initialize subscription arrays.
      // --------------------------------------------------

      this.subscribeClearedValueChangeFn = [];
      this.subscribeUnclearedValueChangeFn = [];

      // --------------------------------------------------
      //  Initialize splits arrays.
      // --------------------------------------------------

      this._splits = myData.splits.map(s => new SplitTransaction(this, s));
    }

    // ==================================================
    //  Object type description.
    // ==================================================

    get constructorName () {
      return 'Transaction';
    }

    // ==================================================
    //  PouchDB JSON data object.
    // ==================================================

    get data () {
      return this._data;
    }

    set data (data) {
      this.splits = data.splits.map(s => new SplitTransaction(this, s));

      // SET CATEGORY
      this._emitCategoryChange(() => {
        this._data.category = data.category;
        this.setMonth();
      });

      // SET VALUE
      if (data.cleared || data.reconciled) {
        this._emitClearedValueChange(data.value - this._data.value);
      } else {
        this._emitUnclearedValueChange(data.value - this._data.value);
      }

      this._emitValueChange(data.value - this._data.value);

      // SET CLEARED
      if ((data.cleared || data.reconciled) !== this.cleared) {
        if (data.cleared || data.reconciled) {
          this._emitUnclearedValueChange(-data.value);
          this._emitClearedValueChange(data.value);
        } else {
          this._emitClearedValueChange(-data.value);
          this._emitUnclearedValueChange(data.value);
        }
      }

      // SET ACCOUNT
      this._emitAccountChange(data.account, this._data.account);

      // SET DATE
      const oldDate = this.month;
      this._date = moment(data.date).toDate();
      this._date.setHours(0, 0, 0, 0);
      this.setMonth();
      this._emitMonthChange(this.month, oldDate);

      // this.splits.forEach(s => {
      //   split._emitMonthChange(this.month, oldDate);
      // });

      // TODO payee

      this._data = data;
    }

    // ==================================================
    //  Value of the transaction.
    // ==================================================

    get value () {
      return this._data.value;
    }

    set value (x) {
      const oldValue = this._data.value;
      this._data.value = x;

      if (this.cleared) {
        this._emitClearedValueChange(x - oldValue);
      } else {
        this._emitUnclearedValueChange(x - oldValue);
      }

      this._emitValueChange(x - oldValue);
      this._emitChange();

      if (this.transfer) {
        const transOldValue = this.transfer._data.value;
        this.transfer._data.value = -x;

        if (this.transfer.cleared) {
          this.transfer._emitClearedValueChange(-x - transOldValue);
        } else {
          this.transfer._emitUnclearedValueChange(-x - transOldValue);
        }

        this.transfer._emitValueChange(-x - transOldValue);
        this.transfer._emitChange();
      }
    }

    // ==================================================
    //  Splits array.
    // ==================================================

    get splits () {
      return this._splits;
    }

    set splits (sArr) {
      let i = 0;
      while (i < this._splits.length) {
        if (sArr[i] && sArr[i].id === this._splits[i].id) {
          this._splits[i].data = sArr[i].data;
          this._splits[i].transfer = sArr[i].transfer;
          i++;
        } else {
          // doesn't exist in order, must have been removed
          if (this._splits[i].transfer) {
            this.removeTransaction && this.removeTransaction(this._splits[i].transfer);
          }

          this.removeTransaction && this.removeTransaction(this._splits[i]);
          this.removeSplit(this._splits[i]);
          // i will be 'incremented' by removing in place
        }
      }

      // if we have additional
      while (i < sArr.length) {
        this.addSplit(sArr[i]);
        this.addTransaction && this.addTransaction(sArr[i]);
        i++;
      }
    }

    // ==================================================
    //  Add split to the transaction.
    // ==================================================

    addSplit (split) {
      this._splits.push(split);
    }

    // ==================================================
    //  Remove split from the transaction.
    // ==================================================

    removeSplit (split) {
      const index = this._splits.indexOf(split);

      if (index !== -1) {
        this._splits.splice(index, 1);
      }
    }

    // ==================================================
    //  Transaction on the other end if this is a
    //  transfer.
    // ==================================================

    get transfer () {
      return this._transfer;
    }

    set transfer (transfer) {
      this._transfer = transfer;
    }

    // ==================================================
    //  Payee for the transaction.
    // ==================================================

    get payee () {
      return this._data.payee;
    }

    set payee (payee) {
      if (payee !== this._data.payee) {
        this._data.payee = payee;
        this._emitChange();
      }
    }

    // ==================================================
    //  Value converted to a positive outlfow if
    //  negative.
    // ==================================================

    get outflow () {
      if (this.value < 0) {
        return Math.abs(this.value);
      }
    }

    set outflow (v) {
      this.value = -v;
    }

    // ==================================================
    //  Value converted to a positive inflow if positive.
    // ==================================================

    get inflow () {
      if (this.value > 0) {
        return this.value;
      }
    }

    set inflow (v) {
      this.value = v;
    }

    // ==================================================
    //  Date of the transaction.
    // ==================================================

    get date () {
      return this._date;
    }

    set date (x) {
      this._setDate(x);

      if (this.transfer) {
        this.transfer._setDate(x);
      }

      this.splits.forEach(s => {
        s._setDateFromParent(x);
      });
    }

    // ==================================================
    //  Set date and update budget info.
    // ==================================================

    _setDate (x) {
      this._data.date = moment(x).format('YYYY-MM-DD');
      const oldDate = this.month;
      this._date = x;
      this._date.setHours(0, 0, 0, 0);

      this.setMonth();

      this._emitMonthChange(this.month, oldDate);
      this._emitChange();
    }

    // ==================================================
    //  Month that the transaction resides in.
    // ==================================================

    get month () {
      return this._month;
    }

    // ==================================================
    //  Set month and update budget info.
    // ==================================================

    setMonth () {
      this._month = moment(this._date);

      if (this.category === 'incomeNextMonth') {
        this._month = this._month.add(1, 'month');
      }

      this._month = this._month.toDate();
    }

    // ==================================================
    //  Transaction category.
    // ==================================================

    get category () {
      return this._data.category;
    }

    set category (x) {
      this._emitCategoryChange(() => {
        this._data.category = x;
        this.setMonth();
      });

      this._emitChange();
    }

    // ==================================================
    //  Transaction account.
    // ==================================================

    get account () {
      return this._data.account;
    }

    set account (x) {
      const oldAccount = this._data.account;
      this._data.account = x;

      this._emitAccountChange(x, oldAccount);
      this._emitChange();
    }

    // ==================================================
    //  Transaction memo.
    // ==================================================

    get memo () {
      return this._data.memo;
    }

    set memo (x) {
      this._data.memo = x;

      if (this.transfer) {
        this.transfer.data.memo = x;
        this.transfer._emitChange();
      }

      this._emitChange();
    }

    // ==================================================
    //  Transaction check number.
    // ==================================================

    get checkNumber () {
      return this._data.checkNumber;
    }

    set checkNumber (x) {
      this._data.checkNumber = x;
      this._emitChange();
    }

    // ==================================================
    //  Whether the transaction is cleared.
    //
    //  Will also call cleared and uncleared subscribers
    //  with the inverse of the current `value` (swapping
    //  the value from cleared to uncleared or vice
    //  versa).
    //
    //  If transaction is reconciled, it's implicitly
    //  cleared.
    // ==================================================

    get cleared () {
      return this._data.cleared || this.reconciled;
    }

    set cleared (x) {
      // Don't do anything if it's the same.
      // If reconciled, cannot change cleared status.
      if (x === this.cleared || this.reconciled) {
        return;
      }

      if (x) {
        this._emitUnclearedValueChange(-this.value);
        this._emitClearedValueChange(this.value);
      } else {
        this._emitClearedValueChange(-this.value);
        this._emitUnclearedValueChange(this.value);
      }

      this._data.cleared = x;
      this._emitChange();
    }

    // ==================================================
    //  Whether the transaction has been reconciled.
    //
    //  A transaction should always be cleared if it has
    //  been reconciled.
    // ==================================================

    get reconciled () {
      return this._data.reconciled;
    }

    set reconciled (x) {
      if (x !== this._data.reconciled) {
        this._data.reconciled = x;
        this._emitChange();
      }
    }

    // ==================================================
    //  Transaction flag color. Acceptable colors:
    //    #cccccc - gray (none)
    //    #ff0000 - red
    //    #faa710 - orange
    //    #e5e500 - yellow
    //    #76b852 - green
    //    #5276b8 - blue
    //    #b852a9 - purple
    // ==================================================

    get flag () {
      return this._data.flag;
    }

    set flag (x) {
      this._data.flag = x;
      this._emitChange();
    }

    // ==================================================
    //  PouchDB object ID.
    // ==================================================

    get _id () {
      return this._data._id;
    }

    // ==================================================
    //  PouchDB object revision.
    // ==================================================

    set _rev (r) {
      this.data._rev = r;
    }

    // ==================================================
    //  Change subscription functions.
    // ==================================================

    subscribe (fn) {
      this.fn = fn;
    }

    subscribeValueChange (fn) {
      this.subscribeValueChangeFn = fn;
    }

    subscribeAccountChange (fn) {
      this.subscribeAccountChangeFn = fn;
    }

    subscribeMonthChange (fn) {
      this.subscribeMonthChangeFn = fn;
    }

    subscribeCategoryChange (beforeFn, afterFn) {
      this.subscribeCategoryChangeBeforeFn = beforeFn;
      this.subscribeCategoryChangeAfterFn = afterFn;
    }

    subscribeClearedValueChange (fn) {
      this.subscribeClearedValueChangeFn.push(fn);
    }

    subscribeUnclearedValueChange (fn) {
      this.subscribeUnclearedValueChangeFn.push(fn);
    }

    subscribePayeeChange (fn) {
      this.subscribePayeeChangeFn = fn;
    }

    // ==================================================
    //  Change unsubscription functions.
    // ==================================================

    unsubscribeClearedValueChange (fn) {
      const index = this.subscribeClearedValueChangeFn.indexOf(fn);

      if (index > -1) {
        this.subscribeClearedValueChangeFn.splice(index, 1);
      } else {
        throw new Error('Subscriber does not exist', fn);
      }
    }

    unsubscribeUnclearedValueChange (fn) {
      const index = this.subscribeUnclearedValueChangeFn.indexOf(fn);

      if (index > -1) {
        this.subscribeUnclearedValueChangeFn.splice(index, 1);
      } else {
        throw new Error('Subscriber does not exist', fn);
      }
    }

    // ==================================================
    //  Change event functions.
    // ==================================================

    _emitChange () {
      return this.fn && this.fn(this);
    }

    _emitAccountChange (newAccount, oldAccount) {
      this.subscribeAccountChangeFn && this.subscribeAccountChangeFn(newAccount, oldAccount);
    }

    _emitMonthChange (newMonth, oldMonth) {
      this.subscribeMonthChangeFn && this.subscribeMonthChangeFn(newMonth, oldMonth);
    }

    _emitCategoryChange (fn) {
      this.subscribeCategoryChangeBeforeFn && this.subscribeCategoryChangeBeforeFn();
      fn();
      this.subscribeCategoryChangeAfterFn && this.subscribeCategoryChangeAfterFn();
    }

    _emitPayeeChange (newPayee, oldPayee) {
      return this.subscribePayeeChangeFn && this.subscribePayeeChangeFn(newPayee, oldPayee);
    }

    _emitValueChange (val) {
      return this.subscribeValueChangeFn && this.subscribeValueChangeFn(val);
    }

    _emitClearedValueChange (val) {
      for (let i = 0; i < this.subscribeClearedValueChangeFn.length; i++) {
        this.subscribeClearedValueChangeFn[i](val);
      }
    }

    _emitUnclearedValueChange (val) {
      for (let i = 0; i < this.subscribeUnclearedValueChangeFn.length; i++) {
        this.subscribeUnclearedValueChangeFn[i](val);
      }
    }

    // ==================================================
    //  Remove from the PouchDB database, as well as any
    /// transfers.
    // ==================================================

    remove () {
      this.splits.forEach(split => {
        split.remove();
      });

      if (this.transfer && !this.transfer._data._deleted) {
        this.transfer._data._deleted = true;
        this.transfer._emitChange();
      }

      if (!this._data._deleted) {
        this._data._deleted = true;
        this._emitChange();
      }
    }

    // ==================================================
    //  Return _data as javascript object.
    // ==================================================

    toJSON () {
      this._data.splits = this.splits.map(s => s.toJSON());
      return this._data;
    }

    // ==================================================
    //  PouchDB object key components.
    // ==================================================

    static get startKey () {
      return `b_${budgetId}_transaction_`;
    }

    static get endKey () {
      return this.startKey + '\uffff';
    }

    static get prefix () {
      return this.startKey;
    }

    static contains (id) {
      return id > this.startKey && id < this.endKey;
    }
  }

  // ==================================================
  //  Return the Transaction class.
  // ==================================================

  return Transaction;
}
