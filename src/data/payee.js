/*
╔══════════════════════════════════════════════════════════════════════════════╗
║ Financier Payee Data Object                                                  ║
╟──────────────────────────────────────────────────────────────────────────────╢
║ Author: Adam Romzek <asromzek@gmail.com>                                     ║
║ Description: Financier Payee data object.                                    ║
╚══════════════════════════════════════════════════════════════════════════════╝
*/

// ##################################################
//  Library imports.
// ##################################################

import uuid from 'uuid';

// ##################################################
//  Class wrapper function. Allows setting global
//  'budgetId' variable for static functions.
// ##################################################

export function Payee (budgetId) {
  //
  // ##################################################
  //  Payee class definition.
  // ##################################################

  class Payee {
    //
    // ==================================================
    //  Constructor.
    // ==================================================

    constructor (data) {
      //
      // --------------------------------------------------
      //  Initialize data object with default values if
      //  none are specified when the object is created.
      // --------------------------------------------------

      const myData = Object.assign({
        name: null,
        autosuggest: true,
        internal: false,
        categorySuggest: null,
        locations: []
      }, data);

      // --------------------------------------------------
      //  Add _id if none exists.
      // --------------------------------------------------

      if (!myData._id) {
        myData._id = Payee.prefix + uuid.v4();
      }

      // --------------------------------------------------
      //  Parse the Payee object ID.
      // --------------------------------------------------

      this.id = myData._id.slice(myData._id.lastIndexOf('_') + 1);

      // --------------------------------------------------
      //  Initialize the local data object.
      // --------------------------------------------------

      this.data = myData;
    }

    // ==================================================
    //  Used when creating a transaction to  indicate
    //  that payee is actualy a payee for a transfer.
    // ==================================================

    get constructorName () {
      return 'Payee';
    }

    // ==================================================
    //  Name of the payee.
    // ==================================================

    get name () {
      return this.data.name;
    }

    set name (n) {
      this.data.name = n;
      this.emitChange();
    }

    // ==================================================
    //  GPS location collection.
    // ==================================================

    get locations () {
      if (!this.data.locations) {
        this.data.locations = [];
        this.emitChange();
      }
      return JSON.parse(JSON.stringify(this.data.locations));
    }

    addLocation (lat, lng, cat) {
      const newId = uuid.v4();

      if (!this.data.locations) { this.data.locations = []; }
      this.data.locations.push({
        id: newId,
        latitude: lat,
        longitude: lng,
        category: cat
      });
      this.emitChange();
      return newId;
    }

    removeLocations (ids) {
      if (!this.data.locations) { this.data.locations = []; }
      this.data.locations = this.data.locations.filter(loc => !ids.includes(loc.id));
      this.emitChange();
    }

    clearLocations () {
      this.data.locations = [];
      this.emitChange();
    }

    // ==================================================
    //  If this is a "special" payee that should not be
    //  removable.
    // ==================================================

    get internal () {
      return this.data.internal;
    }

    // ==================================================
    //  Whether the payee is set to autosuggest.
    // ==================================================

    get autosuggest () {
      return this.data.autosuggest;
    }

    set autosuggest (n) {
      this.data.autosuggest = n;
      this.emitChange();
    }

    // ==================================================
    //  The category ID last used with this payee.
    // ==================================================

    get categorySuggest () {
      return this.data.categorySuggest;
    }

    set categorySuggest (n) {
      if (this.data.categorySuggest !== n) {
        this.data.categorySuggest = n;
        this.emitChange();
      }
    }

    // ==================================================
    //  PouchDB object ID.
    // ==================================================

    get _id () {
      return this.data._id;
    }

    // ==================================================
    //  Change subscription function.
    // ==================================================

    subscribe (fn) {
      this.fn = fn;
    }

    // ==================================================
    //  Change event function.
    // ==================================================

    emitChange () {
      return this.fn && this.fn(this);
    }

    // ==================================================
    //  Remove from the PouchDB database.
    // ==================================================

    remove () {
      if (this.internal) {
        throw new Error('Cannot remove internal payee!');
      }

      this.data._deleted = true;
      return this.emitChange();
    }

    // ==================================================
    //  Return _data as javascript object.
    // ==================================================

    toJSON () {
      return this.data;
    }

    // ==================================================
    //  PouchDB object key components.
    // ==================================================

    static get startKey () {
      return `b_${budgetId}_payee_`;
    }

    static get endKey () {
      return this.startKey + '\uffff';
    }

    static get prefix () {
      return this.startKey;
    }

    static contains (id) {
      return id > this.startKey && id < this.endKey;
    }
  }

  // ==================================================
  //  Return the Payee class.
  // ==================================================

  return Payee;
}
