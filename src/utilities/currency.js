/*
╔══════════════════════════════════════════════════════════════════════════════╗
║ Financier Currency Digits & Symbols                                          ║
╟──────────────────────────────────────────────────────────────────────────────╢
║ Author: Adam Romzek <asromzek@gmail.com>                                     ║
║ Description: Financier currency digit and symbol library.                    ║
╚══════════════════════════════════════════════════════════════════════════════╝
*/

export class Currency {
  //
  // ==================================================
  //  Constructor.
  // ==================================================

  constructor () {
    this._digits = 2;
    this._symbol = '';
  }

  // ==================================================
  //  The number of decimal places that currency values
  //  should have.
  // ==================================================

  get digits () {
    return this._digits;
  }

  set digits (d) {
    this._digits = d;
  }

  // ==================================================
  //  The symbol character to add to currency values.
  // ==================================================

  get symbol () {
    return this._symbol;
  }

  set symbol (s) {
    this._symbol = s;
  }
}
