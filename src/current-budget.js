import { Subject } from 'rxjs/Subject';
import { DbController } from './db-controller';

export function CurrentBudget (db, currency) {
  let id;
  let budgetManager;
  let dbCtrl;

  const $budgetOpened = new Subject();
  const $budgetClosed = new Subject();

  function loadBudget (budgetId) {
    unloadBudget();

    id = budgetId;
    budgetManager = db.budget(id);

    return Promise.all([
      budgetManager.budget(),
      budgetManager.categories.all(),
      budgetManager.masterCategories.all(),
      budgetManager.payees.all(),
      db.budgets.get(id)
    ]).then(([monthManager, categories, masterCategories, payees, budget]) => {
      monthManager.propagateRolling(Object.keys(categories));
      dbCtrl = new DbController(id, db, budget, budgetManager, monthManager, categories, masterCategories, payees, currency);
      $budgetOpened.next(dbCtrl.budget.name);
      this.monthMemory = new Date();
      return dbCtrl;
    }).catch(e => {
      throw e;
    });
  }

  function unloadBudget () {
    if (dbCtrl !== undefined) {
      id = null;
      dbCtrl.destroy();
      dbCtrl = undefined;
      $budgetClosed.next(null);
    }
  }

  function budgetMgr () {
    return budgetManager;
  }

  function budgetId () {
    return id;
  }

  return {
    budgetId,
    budgetMgr,
    dbCtrl,
    loadBudget,
    unloadBudget,
    $budgetOpened,
    $budgetClosed
  };
}
